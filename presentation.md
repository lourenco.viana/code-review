---
theme: uncover
_class: lead
paginate: true
backgroundColor: #fff
backgroundImage: url('https://marp.app/assets/hero-background.svg')

---
![bg left:100% 100%](https://gitlab.com/lourenco.viana/code-review/-/raw/main/presentation/00.0-code-review.png)

---
![bg left:100% 60%](https://gitlab.com/lourenco.viana/code-review/-/raw/main/presentation/00.1-14-leis-do-code-review.png)

---

<style scoped>
h1 {
  color: black;
}
</style>
# Regra 1
## O código funciona?

---
![bg left:100% 100%](https://gitlab.com/lourenco.viana/code-review/-/blob/main/presentation/01.1-code-does-not-work.png)

---
![bg left:100% 100%](https://gitlab.com/lourenco.viana/code-review/-/raw/main/presentation/01.2-bogosort.png)

---
![bg left:100% 60%](https://gitlab.com/lourenco.viana/code-review/-/raw/main/presentation/01.3-meme.jpg)

---
# Regra 2
## É compreensível?

---
![bg left:100% 100%](https://gitlab.com/lourenco.viana/code-review/-/raw/main/presentation/02.1-joke-1.png)

---
![bg left:100% 50%](https://gitlab.com/lourenco.viana/code-review/-/raw/main/presentation/02.2-aimeucerebro.jpg)

---
![bg left:100% 100%](https://gitlab.com/lourenco.viana/code-review/-/raw/main/presentation/02.2-sucessoException.png)

---
# Regra 3
## Confere com as convenções?

---
![bg left:100% 100%](https://gitlab.com/lourenco.viana/code-review/-/raw/main/presentation/03.1-sad-sql-noises.gif)

---
# Regra 4
## Existe código redundante ou duplicado?

---

![bg left:100% 50%](https://gitlab.com/lourenco.viana/code-review/-/raw/main/presentation/04.1-code-duplication.png)

---

![bg left:100% 100%](https://gitlab.com/lourenco.viana/code-review/-/raw/main/presentation/04.2-porque.png)

---
# Regra 5
## O código é modular?

---

![bg left:100% 80%](https://gitlab.com/lourenco.viana/code-review/-/raw/main/presentation/05.1-porque...continuacao.png)

---
# Regra 6
## Existem variáveis fora do escopo correto?

*global, public e outros efeitos não intencionais na classe

---

![bg left:100% 80%](https://gitlab.com/lourenco.viana/code-review/-/raw/main/presentation/06.1-porque...prequel.png)

---
# Regra 7
## Existe código comentado?

---

![bg left:100% 90%](https://gitlab.com/lourenco.viana/code-review/-/raw/main/presentation/07.1-tartaruga-na-arvore.png)

---
# Regra 8
## Loops possuem fim e condições para fim claras?

---

![bg left:100% 60%](https://gitlab.com/lourenco.viana/code-review/-/raw/main/presentation/08.1-bizarre-loop.png)

---
# Regra 9
## Existe uma library que faz tudo isso?

*ou isso pode virar uma library?

---

![bg left:100% 100%](https://gitlab.com/lourenco.viana/code-review/-/raw/main/presentation/09.1-made-a-mapper.png)

---

![bg left:100% 100%](https://gitlab.com/lourenco.viana/code-review/-/raw/main/presentation/09.2-you-are-not-special.png)

---
# Regra 10
## Existe código de debug?

---

![bg left:100% 100%](https://gitlab.com/lourenco.viana/code-review/-/raw/main/presentation/10.1-passou-aqui.png)

---
# Regra 11
## Feedbacks
Você precisa dar feedbacks!

---

Pergunte a Raquel Ohashi como dar feedbacks

---

# Regra 12
## Segurança
Algo está sendo expostos e não deveria?

---

# Regra 13
## Teste
Os testes implementados fazem sentido

*ver todas as outras regras

---

# Regra 14
## documentacao

Foi feita? É necessária?

---

![bg left:100% 100%](https://gitlab.com/lourenco.viana/code-review/-/raw/main/presentation/14.1-vaca-do-quico.png)