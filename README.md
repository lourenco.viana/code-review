# code review

Repositório para prática de code review

## Indice
- [resumo de como fazer](como-fazer.md)
- [CheckList do code review](checklist.md)
- [Tipos de branch](branchtypes.md)
- [Apresentação](presentation.md)
  - [Roteiro](presentation-script.md)
- [Marp](https://marp.app/)


## Buildando Apresentação

- `npm run build`: gera imagens no diretório **dist/images**
- `npm run build:pptx`: gera apresentação office no diretório **dist**
- `npm run build:html`: gera página html no diretório **dist**