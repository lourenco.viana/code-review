# Reviwer
- planejar de 60 a 90 minutos para realizar uma review por dia
- nao revisar mais de 200 linhas de codigo de uma vez
- fazer pausas de 20 minutos para cada sessão de review
- verificar se as fix realmente corrigem o problema
- usar a ferramente como team building não como capataz
- utilizar a [checklist](checklist.md)

# Reviwed
- evitar segurar muito código para enviar pra review (commits diários no draft se não der pra terminar em 1 dia)
  - o reviwer precisa de tempo e esforço para avaliar o código, não o sobrecarregue
  - lembrar de tirar do draft quando o código está ok!
- criar as branchs seguindo os padrões de nomenclatura [tipo](branchtypes.md)/descricao-breve
- setar um aprovador no merge request para que o acompanhamento seja mais 'automático'